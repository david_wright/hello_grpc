workspace(name = "hello_grpc")

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:git.bzl", "git_repository")

# Rules for apple build
http_archive(
    name = "build_bazel_rules_apple",
    sha256 = "12865e5944f09d16364aa78050366aca9dc35a32a018fa35f5950238b08bf744",
    url = "https://github.com/bazelbuild/rules_apple/releases/download/0.34.2/rules_apple.0.34.2.tar.gz",
)
load("@build_bazel_rules_apple//apple:repositories.bzl", "apple_rules_dependencies")
apple_rules_dependencies()

# Rules for grpc
http_archive(
    name = "com_github_grpc_grpc",
    sha256 = "d6cbf22cb5007af71b61c6be316a79397469c58c82a942552a62e708bce60964",
    urls = [
        "https://github.com/grpc/grpc/archive/refs/tags/v1.46.3.tar.gz",
    ],
    strip_prefix = "grpc-1.46.3",
)
load("@com_github_grpc_grpc//bazel:grpc_deps.bzl", "grpc_deps")
grpc_deps()
load("@com_github_grpc_grpc//bazel:grpc_extra_deps.bzl", "grpc_extra_deps")
grpc_extra_deps()

# Autocompletion for IDEs compatible with clangd & compile_commands.json
git_repository(
    name = "hedron_compile_commands",
    remote = "https://github.com/hedronvision/bazel-compile-commands-extractor",
    commit = "93dc21a40e091836f16e83c3419858d928691566",
    shallow_since = "1653716697 -0700",
    recursive_init_submodules = True
)
load("@hedron_compile_commands//:workspace_setup.bzl", "hedron_compile_commands_setup")
hedron_compile_commands_setup()

# Boost
_RULES_BOOST_COMMIT = "789a047e61c0292c3b989514f5ca18a9945b0029"

http_archive(
    name = "com_github_nelhage_rules_boost",
    sha256 = "c1298755d1e5f458a45c410c56fb7a8d2e44586413ef6e2d48dd83cc2eaf6a98",
    strip_prefix = "rules_boost-%s" % _RULES_BOOST_COMMIT,
    urls = [
        "https://github.com/nelhage/rules_boost/archive/%s.tar.gz" % _RULES_BOOST_COMMIT,
    ],
)

load("@com_github_nelhage_rules_boost//:boost/boost.bzl", "boost_deps")
boost_deps()

# GTest
http_archive(
  name = "com_google_googletest",
  urls = ["https://github.com/google/googletest/archive/refs/tags/release-1.11.0.tar.gz"],
  strip_prefix = "googletest-release-1.11.0",
)
