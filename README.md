# hello_grpc

A small project to get started with grpc and bazel

## Development Environment Setup
- To resolve the error `DEVELOPER_DIR not set` on OSX:
  - Option 1: `echo -e "export DEVELOPER_DIR=\"/Applications/Xcode.app/Contents/Developer\"\nexport SDKROOT=\"/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk\"" >> ~/.zshrc` (or bashrc)
  - Option 2: Add `build --action_env=BAZEL_USE_CPP_ONLY_TOOLCHAIN=1` to the `.bazelrc`.
    - I couldn't get this to work with `hedron_compile_commands` for some reason, and dependencies failed to build.
